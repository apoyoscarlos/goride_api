<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Http\Requests\UsuarioValidator;
use Illuminate\Support\Facades\DB;
use Exception;

class UsuarioController extends Controller
{
    protected $validate;
    public function __construct(UsuarioValidator $validate){
        $this->validate = $validate;
    }
   
    public function store(Request $request){
        $validation = $this->validate->store($request);
        $data = $request->all();

        if( $validation  !== true){
            return response()->json(['error'=> $validation->original], 403);
        }
        $data["password"] = bcrypt($data['password']);
        $item = Usuario::create($data);
        return response()->json(['id'=> $item->id], 200);
    }

    public function get($id){
        $item = Usuario::find($id);
        if(is_null($item)){
            return response()->json( ['error'=> "No se encontro el registro con id ".$id], 403);
        }
       
        return response()->json($item, 200);
    }

    public function update(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->update($request);
            $data = $request->all();

            if( $validation  !== true){
                return response()->json(['error'=> $validation->original], 403);
            }
            $item = Usuario::where('id', $request->id)->update($data);
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->errorInfo[2];
        }
        return response()->json($response, $codigo);
    }

    public function destroy($id){
        $item = Usuario::find($id);
        if(is_null($item)){
            return response()->json( ['error'=> "No se encontro el registro con id ".$id], 403);
        } 
        
        $item->delete();
        return response()->json(['message'=> 'Operacion Exitosa'], 200);
    }

    public function find(Request $request){
        $items = [];
        $order = 'desc';
        $order_by = 'id';

        $validation = $this->validate->find($request);
        if( $validation  !== true){
            return response()->json(['error'=> $validation->original], 403);
        }
       
        //Comienza el Query
        $query = DB::table('usuario');
        if(!is_null($request->nombre)){
            $query->where('nombre','like','%'. $request->nombre .'%');
        }
        if(!is_null($request->apellido)){
            $query->where('apellido','like','%'. $request->apellido .'%');
        }
        if(!is_null($request->celular)){
            $query->where('celular','like','%'. $request->celular .'%');
        }
        if(!is_null($request->correo)){
            $query->where('correo','like','%'. $request->correo .'%');
        }
        if(!is_null($request->rol)){
            $query->where('rol_id','=', $request->rol );
        }

        //Parametros de paginacion y orden
        if(!is_null($request->order)){
            $order = $request->order;
        }
        if(!is_null($request->order_by)){
            $order_by = $request->order_by;
        }
        $query->orderBy($order_by, $order);

        if(is_null($request->paginate) || $request->paginate == 'true' ){
            $resultados = 10;
            if(!is_null($request->items_to_show)){
                $resultados = $request->items_to_show;
            }
            $items = $query->paginate($resultados);
        }
        else{
            $items = $query->get();
            return response()->json(['data' => $items], 200);
        }
        
        return response()->json($items, 200);
    }
}
