<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_compra', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('compra_id')->unsigned()->index();
            $table->bigInteger('lugar_id')->unsigned()->index();
            $table->bigInteger('pasajero_id')->unsigned()->index();

            $table->foreign('compra_id')->references('id')->on('compra')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('lugar_id')->references('id')->on('lugares')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('pasajero_id')->references('id')->on('pasajero')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_compra');
    }
}
