<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfiguracionEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracion_email', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('puerto')->nullable();
            $table->string('servidor', 200)->nullable();
            $table->integer('ssl')->nullable();
            $table->string('correo', 200)->nullable();
            $table->string('password', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuracion_email');
    }
}
