<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrigenDestinoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('origen_destino', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('precio', 9,2)->nullable();
            $table->integer('tipo')->nullable();
            $table->bigInteger('rutaOrigen_id')->unsigned()->index();
            $table->bigInteger('rutaDestino_id')->unsigned()->index();
            $table->dateTime('fechaSalida', 0);
            $table->dateTime('fechaFin', 0)->nullable();
            $table->boolean('activo')->default(true);
            $table->timestamps();

            $table->foreign('rutaOrigen_id')->references('id')->on('rutas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('rutaDestino_id')->references('id')->on('rutas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('origen_destino');
    }
}
