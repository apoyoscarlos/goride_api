<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compra', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fechaCompra',0)->nullable();
            $table->tinyInteger('esInvitado')->nullable();
            $table->tinyInteger('facturado')->nullable();
            $table->tinyInteger('pagado')->nullable();
            $table->bigInteger('origenDestino_id')->unsigned()->index();
            $table->dateTime('fechaSalida',0);
            $table->dateTime('fechaRegreso',0)->nullable();
            $table->bigInteger('usuario_id')->nullable()->unsigned()->index();
            $table->timestamps();

            $table->foreign('origenDestino_id')->references('id')->on('origen_destino')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuario')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compra');
    }
}
