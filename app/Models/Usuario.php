<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Usuario extends Authenticatable
{
    use Notifiable, HasApiTokens;
    
    protected $table = 'usuario';
    protected $fillable = [
        'nombre',
        'apellido',
        'celular',
        'correo',
        'password',
        'rol_id'
    ];

    protected $hidden = ['password'];

    public function rol(){
        return $this->hasOne(Rol::class);
    }
}
