<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tarjeta;
use App\Http\Requests\TarjetaValidator;
use Illuminate\Support\Facades\DB;
use Exception;

class TarjetaController extends Controller
{
    protected $validate;
    public function __construct(TarjetaValidator $validate){
        $this->validate = $validate;
    }
   
    public function store(Request $request){
        $validation = $this->validate->store($request);
        $data = $request->all();

        if( $validation  !== true){
            return response()->json(['error'=> $validation->original], 403);
        }
        
        $item = Tarjeta::create($data);
        return response()->json(['id'=> $item->id], 200);
    }

    public function get($id){
        $item = Tarjeta::find($id);
        if(is_null($item)){
            return response()->json( ['error'=> "No se encontro el registro con id ".$id], 403);
        }
        $item->usuario;
        return response()->json($item, 200);
    }

    public function update(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->update($request);
            $data = $request->all();

            if( $validation  !== true){
                return response()->json(['error'=> $validation->original], 403);
            }
            $item = Tarjeta::where('id', $request->id)->update($data);
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->errorInfo[2];
        }
        return response()->json($response, $codigo);
    }

    public function destroy($id){
        $item = Tarjeta::find($id);
        if(is_null($item)){
            return response()->json( ['error'=> "No se encontro el registro con id ".$id], 403);
        } 
        
        $item->delete();
        return response()->json(['message'=> 'Operacion Exitosa'], 200);
    }

    public function find(Request $request){
        $items = [];
        $order = 'desc';
        $order_by = 'id';

        $validation = $this->validate->find($request);
        if( $validation  !== true){
            return response()->json(['error'=> $validation->original], 403);
        }
       
        //Comienza el Query
        $query = DB::table('tarjeta as t')
                ->join('usuario as u', 't.usuario_id', '=', 'u.id')
                ->select('t.*', 'u.nombre as nombre_usuario', 'u.apellido as apellido_usuario', 'u.correo as correo_usuario');
        
        if(!is_null($request->usuario_id)){
            $query->where('t.usuario_id','=', $request->usuario_id);
        }
        if(!is_null($request->referenciaExterna_id)){
            $query->where('t.referenciaExterna_id','=',$request->referenciaExterna_id);
        }
        if(!is_null($request->correo_usuario)){
            $query->where('u.correo','like','%'. $request->correo_usuario .'%');
        }

        //Parametros de paginacion y orden
        if(!is_null($request->order)){
            $order = $request->order;
        }
        if(!is_null($request->order_by)){
            $request->order_by == 'correo_usuario' ? $order_by= 'u.correo': $order_by= $request->order_by;
        }
        $query->orderBy($order_by, $order);

        if(is_null($request->paginate) || $request->paginate == 'true' ){
            $resultados = 10;
            if(!is_null($request->items_to_show)){
                $resultados = $request->items_to_show;
            }
            $items = $query->paginate($resultados);
        }
        else{
            $items = $query->get();
            return response()->json(['data' => $items], 200);
        }
        
        return response()->json($items, 200);
    }
}
