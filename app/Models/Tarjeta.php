<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Tarjeta extends Model
{
    protected $table = 'tarjeta';
    protected $fillable = [
        'referenciaExterna_id',
        'usuario_id'
    ];

    public function usuario():BelongsTo {
        return $this->belongsTo(Usuario::class);
    }
}
