<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/rechazado', 'AuthController@rechazado')->name('rechazado');

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'AuthController@login');
});


Route::group(['prefix' => 'usuarios'], function () {
    Route::get('/find', 'UsuarioController@find');
    Route::get('/{id}', 'UsuarioController@get');
    Route::post('/', 'UsuarioController@store');
    Route::put('/', 'UsuarioController@update');
    Route::delete('/{id?}', 'UsuarioController@destroy');
});

Route::group(['prefix' => 'pasajeros'], function () {
    Route::get('/find', 'PasajeroController@find');
    Route::get('/{id}', 'PasajeroController@get');
    Route::post('/', 'PasajeroController@store');
    Route::put('/', 'PasajeroController@update');
    Route::delete('/{id?}', 'PasajeroController@destroy');
});



/* Rutas que requieren estar autentificado */
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('auth/logout', 'AuthController@logout');

    Route::group(['prefix' => 'tarjetas'], function () {
        Route::get('/find', 'TarjetaController@find');
        Route::get('/{id}', 'TarjetaController@get');
        Route::post('/', 'TarjetaController@store');
        Route::put('/', 'TarjetaController@update');
        Route::delete('/{id}', 'TarjetaController@destroy');
    });
});