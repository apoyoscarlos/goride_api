<?php

namespace App\Http\Requests;

use Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PasajeroValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre'    => 'required|string|max:200',
            'apellido'  => 'required|string|max:200',
            'correo'    => 'nullable|string|max:200|email',
            'celular'   => 'nullable|string|max:45',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'        => 'required|integer|exists:pasajero,id',
            'nombre'    => 'required|string|max:200',
            'apellido'  => 'required|string|max:200',
            'correo'    => 'nullable|string|max:200|email',
            'celular'   => 'nullable|string|max:45',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function find(Request $request){
        $validator = Validator::make($request->all(), [
            'nombre'    => 'nullable|string',
            'apellido'  => 'nullable|string',
            'correo'    => 'nullable|string',
            'celular'   => 'nullable|string',
            'order'     => ['nullable','string', Rule::in(['asc', 'desc'])],
            'order_by'  => ['nullable','string', Rule::in(['id', 'nombre', 'apellido', 'correo','celular' ])],
            'items_to_show'=> 'nullable|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
}
