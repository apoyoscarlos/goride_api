<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrereservaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prereserva', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('lugar_id')->unsigned()->index();
            $table->bigInteger('origenDestino_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('lugar_id')->references('id')->on('lugares')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('origenDestino_id')->references('id')->on('origen_destino')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prereserva');
    }
}
