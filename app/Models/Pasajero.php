<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pasajero extends Model
{
    protected $table = 'pasajero';
    protected $fillable = [
        'nombre',
        'apellido',
        'celular',
        'correo',
    ];
}
