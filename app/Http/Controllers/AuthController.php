<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UsuarioValidator;
use App\Models\Usuario; 
use Carbon\Carbon;
use Exception;

class AuthController extends Controller
{
    
    protected $validate;
    public function __construct(UsuarioValidator $validate){
        $this->validate = $validate;
    }

    public function login(Request $request){
        $validation = $this->validate->login($request);
     
        if( $validation  !== true){
            return response()->json(['error'=> $validation->original], 403);
        }

        if(Auth::attempt(['correo' => request('correo'), 'password' => request('password')])){
            $user = Auth::user();$tokenResult =  $user->createToken('Personal Access Token');
            $tokenResult =  $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addDays(1);
            
            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addDays(1);
            $token->save();

            return response()->json([
                'user' => $user,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ], 200);
        }
        else{
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    public function logout(Request $request){
        if (Auth::user()) {   
            $user = Auth::user()->token();
            $user->revoke();
    
            return response()->json(['message' => 'Operación exitosa'], 200);
        }
        else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
         
    }

    public function rechazado(){
        return response()->json(['error' => 'Unauthorized'], 401);
    }
}
