<?php

namespace App\Http\Requests;

use Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TarjetaValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'usuario_id'            => 'required|integer|exists:usuario,id',
            'referenciaExterna_id'  => 'required|string'
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'                    => 'required|integer|exists:tarjeta,id',
            'usuario_id'            => 'required|integer|exists:usuario,id',
            'referenciaExterna_id'  => 'required|string'
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function find(Request $request){
        $validator = Validator::make($request->all(), [
            'usuario_id'            => 'nullable|string',
            'referenciaExterna_id'  => 'nullable|string',
            'order'     => ['nullable','string', Rule::in(['asc', 'desc'])],
            'order_by'  => ['nullable','string', Rule::in(['usuario_id', 'referenciaExterna_id','correo_usuario' ])],
            'items_to_show'=> 'nullable|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
}
