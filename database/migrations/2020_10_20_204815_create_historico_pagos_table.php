<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricoPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_pagos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tarjeta_id')->unsigned()->index();
            $table->dateTime('fechaSalida', 0);
            $table->float('pago', 9,2);
            $table->text('status');
            $table->timestamps();

            $table->foreign('tarjeta_id')->references('id')->on('usuario')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_pagos');
    }
}
